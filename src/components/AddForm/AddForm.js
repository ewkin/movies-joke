import React, {Component} from 'react';

class AddForm extends Component {
    render() {
        return (
            <div>
                <div className="taskForm">
                    <form action="" onSubmit={this.props.handleSubmit}>
                        <input type="text" value={this.props.value} placeholder= 'Add new movie' onChange={this.props.handleChange}/>
                        <button type="submit">Add</button>
                    </form>
                </div>
            </div>
        );
    }
}

export default AddForm;