import React, {Component} from 'react';

class MovieList extends Component {

    componentDidUpdate() {
        console.log('[movieList] DidUpdate');
    }

    shouldComponentUpdate(nextProps, nextState) {
        console.log('[movieList] ShouldUpdate');
        return nextProps.movie !==this.props.movie
    }


    render() {
        return (
            <div className="task">
                <p>{this.props.movie}</p>
                <button onClick={this.props.handleDelete}>Delete</button>
            </div>
        );
    }
}

export default MovieList;