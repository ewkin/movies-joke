import React, {useEffect, useState} from 'react';

const Jokes = () => {
    const [jokes, setJokes] = useState([]);

    const url ='https://api.chucknorris.io/jokes/random';

    useEffect(()=>{
        const fetchData = async () =>{
            const response = await  fetch (url);
            if(response.ok){
                const jokes= await response.json();
                setJokes(jokes);
            }
        }
        fetchData().catch(console.error);
    },[]);

    console.log(jokes)

    return (
        <div style={{width: '50%'}}>
            {jokes.value}
        </div>
    );
};

export default Jokes;