import React, {Component} from 'react';
import AddForm from "../../components/AddForm/AddForm";
import MovieList from "../../components/MovieList/MovieList";

class Movies extends Component {
    constructor(props) {
        super(props);
        this.state = {
            movies: [
                {title: 'Movie name#1', id: '1'},
                {title: 'Movie name#2', id: '2'},
            ],
            id: 3,
            value: ''
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

    };

    handleChange(event) {
        this.setState({value: event.target.value});
    };

    handleSubmit(event) {
        if(this.state.value.length>0){
            this.setState({movies: [...this.state.movies, {title: this.state.value, id: this.state.id}], id: this.state.id+1 , value:''});
        }
        event.preventDefault();
    };

    handleDelete (id) {
        let index = this.state.movies.findIndex(movie => movie.id === id);
        const moviesCopy = [...this.state.movies];
        moviesCopy.splice(index, 1);
        this.setState({movies: moviesCopy});
    }

    render() {
        return (
            <div style={{width: '50%'}}>
                <AddForm
                    value={this.state.value}
                    handleChange={this.handleChange}
                    handleSubmit={this.handleSubmit}
                />
                To watch list:

                {this.state.movies.map(movie => (
                    <MovieList
                        key={movie.id}
                        movie={movie.title}
                        handleDelete={()=>this.handleDelete(movie.id)}

                    />
                ))}
            </div>
        );
    }
}

export default Movies;