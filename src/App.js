import React from "react";
import './App.css';
import Movies from "./containers/Movies/Movies";
import Jokes from "./containers/Jokes/Jokes";

function App() {
  return (
    <div className="App">
      <Movies/>
      <Jokes/>
    </div>
  );
}

export default App;
